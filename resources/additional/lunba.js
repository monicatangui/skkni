
    $(document).ready(function() {

        // auto managing navbar
        var url = window.location.href;

        $('.sidebar-menu li').each(function() {

            if ($(this).children('a').attr('href') == url) {

                $(this).addClass('active');
                $(this).parents('li.treeview').addClass('active');

            }

        });
        // -- managing navbar


        // auto managing breadcrumb
        var i = 0;
        var menus = [];
        var links = [];

        $('.sidebar-menu li.active').each(function() {

            menus[i] = $(this).contents().children('span').text();
            links[i] = $(this).children('a').attr('href');

            $('ol.breadcrumb').append('<li><a href="'+links[i]+'">'+menus[i]+'</a></li>');
            $('.content-header').append('<h1>'+menus[i]+'</h1>');

            i++;

        })

        // -- managing breadcrumb

    });


    /** mengatur navbar dengan ketika muncul tampilan child dari sebuah tampilan parent */
    function navbar_dynamic(nav_name) {

        // managing navbar
        var url = window.location.href;

        $('.sidebar-menu li').each(function() {

          if ($(this).contents().children('span').text() == nav_name) {

              $(this).addClass('active');

          }

        });
        // -- managing navbar

    }
