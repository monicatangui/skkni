<?php

    defined('BASEPATH') or exit('Direct access script is not allowed');

    class Data extends CI_Controller {

        function __construct()
        {
            parent::__construct();
            $this->load->model('input_peserta');
        }

        public function index()
        {
            $data['result'] = $this->input_peserta->get();
            $data['view_file_path'] = 'input.php';
            $this->load->view('defaults/layout', $data);
        }

        /**
         * Untuk input data peserta
         *
         */
        public function input()
        {
            $this->form_validation->set_rules($this->config->item('input_data'));

            // cek validasi form
            if ($this->form_validation->run() !== TRUE) {
                $this->session->set_flashdata('error', alert('gagal', $this->form_validation->error_string()));
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->input_peserta->simpan($_POST);
                $this->session->set_flashdata('error', alert('berhasil', 'Berhasil Menambahkan Data'));
            }

            redirect('Data');
        }

        /**
         * Untuk mengedit peserta
         *
         */
        public function edit()
        {
            $this->form_validation->set_rules($this->config->item('input_data'));

            // cek validasi form
            if ($this->form_validation->run() !== TRUE) {
                $this->session->set_flashdata('error', alert('gagal', $this->form_validation->error_string()));
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->input_peserta->edit();
                $this->session->set_flashdata('error', alert('mengedit', 'Berhasil Mengubah Data'));
            }

            redirect('Data');
        }

        /**
         * Untuk menghapus peserta
         *
         */
        public function hapus()
        {
            $this->input_peserta->delete();
            $this->session->set_flashdata('error', alert('menghapus', 'Berhasil Menghapus Data'));
            redirect('Data');
        }

    }
