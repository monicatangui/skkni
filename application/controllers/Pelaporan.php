<?php

    defined('BASEPATH') or exit('Direct access script is not allowed');

    class Pelaporan extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->model('input_peserta');
        }

        public function index()
        {
            $data['result'] = $this->input_peserta->get_laporan();
            $data['view_file_path'] = 'pelaporan.php';
            $this->load->view('defaults/layout', $data);
        }

    }
