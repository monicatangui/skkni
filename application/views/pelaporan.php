<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
        <i class="fa fa-arrows"></i>&nbsp;
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <?php

    // flashdata error atau sukses
    if (! empty($this->session->flashdata('error'))) {
      echo $this->session->flashdata('error');
    }
  ?>
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Jumlah Peserta Berdasarkan Tanggal Lahir</h3>
        <div class="box-tools pull-right">

        </div>
      </div>
      <div class="box-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Tanggal Lahir</th>
                    <th style="text-align: center;">Jumlah Peserta</th>
                </tr>
            </thead>
            <tbody>
            <?php if (! empty($result)) :?>
                <?php foreach($result as $row) : ?>
                  <tr>
                      <td align="center"><?php echo $row['id'] ?></td>
                      <td align="center"><?php echo $row['tanggal_lahir'] ?></td>
                      <td align="center">
                          <label class="label label-lg label-primary" style="font-size: 12px;"><?php echo $row['jumlah'] ?></label>
                      </td>
                  </tr>
                <?php endforeach ?>
            <?php endif ?>
            </tbody>
        </table>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
